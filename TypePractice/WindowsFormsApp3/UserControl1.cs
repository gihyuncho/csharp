﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class UserControl1 : UserControl
    {
        private bool doesStartType;
        public event EventHandler StartType;
        public event EventHandler<CheckAnswerEventArgs> ChkAnswer;
        public UserControl1()
        {
            InitializeComponent();
            doesStartType = false;
            
        }
        public void LoadSentence(string sentence)
        {
            doesStartType=false;
            textBox1.Text = sentence;
            textBox2.MaxLength = sentence.Length;
            
        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            if (!doesStartType)
            {
                StartType(this,null);
                doesStartType = true;
            }

            int numOfMatchedChar = textBox2.Text.Length;
            
            for (int i = 0; i < textBox2.Text.Length; ++i)
                if (textBox1.Text[i] != textBox2.Text[i])
                    --numOfMatchedChar;

            double accuracy = (double)numOfMatchedChar / textBox1.Text.Length;

            if(numOfMatchedChar == textBox2.Text.Length)
                textBox1.BackColor = Color.LightGreen;
            else
                textBox1.BackColor = Color.PaleVioletRed;

            if (numOfMatchedChar == textBox1.Text.Length)
                ChkAnswer(this, new CheckAnswerEventArgs(true, accuracy));
            else
                ChkAnswer(this, new CheckAnswerEventArgs(false, accuracy));

        }

        public class CheckAnswerEventArgs
        {
            public CheckAnswerEventArgs(bool isCorrect,double accuracy)
            {
                IsCorrect = isCorrect;
                Accuracy = accuracy;
            }
            public bool IsCorrect { get; set; }
            public double Accuracy { get; set; }
        }
    }
}
