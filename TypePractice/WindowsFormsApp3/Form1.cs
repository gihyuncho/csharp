﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{

    public partial class Form1 : Form
    {
        private const int TIME_INIT_VALUE = -2;
        private const string sentence = "Hello World!";
        private int timeElapsed;
        private bool isCorrect;
        private double accuracy;
        public Form1()
        {
            InitializeComponent();
            timeElapsed = TIME_INIT_VALUE;
            userControl11.StartType += StartTimer;
            userControl11.ChkAnswer += CheckAnswer;
        }

        private void StartTimer(object sender, EventArgs e)
        {
            if (timeElapsed != TIME_INIT_VALUE)
                return;

                ++timeElapsed;
        }

        private void CheckAnswer(object sender, UserControl1.CheckAnswerEventArgs e)
        {
            isCorrect = e.IsCorrect;
            accuracy = e.Accuracy;
            label2.Text = "accuracy: "+e.Accuracy.ToString();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            userControl11.LoadSentence(sentence);
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {

            if (timeElapsed == TIME_INIT_VALUE) return;
            timeElapsed++;
            label1.Text = "TimeElapsed: "+timeElapsed.ToString();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show((isCorrect ? "Correct! :)" : "Wrong.. :(") + $"\nElapsedTime: {timeElapsed}\naccuracy: {accuracy*100}%");
        }
    }
}
