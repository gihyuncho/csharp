﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Data
{
    public class GenreData : EntityData<Genre>
    {
        public List<Genre> GetAllOrderByName()
        {
            using (ChinookEntities context = new ChinookEntities())
            {
                var query = from x in context.Genres
                            orderby x.Name
                            select x;

                return query.ToList();
            }
        }

        public Genre GetByPK(int genreId)
        {
            using (ChinookEntities context = new ChinookEntities())
            {
                return context.Genres.FirstOrDefault(x => x.GenreId == genreId);
            }
        }

        public void DeleteByPK(int genreId)
        {
            Genre entity = GetByPK(genreId);

            if (entity == null)
                return;

            Delete(entity);
        }
    }
}
