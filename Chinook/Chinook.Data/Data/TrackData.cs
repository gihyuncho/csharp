﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Data
{
    public class TrackData:EntityData<Track>
    {
        public List<Track> GetAllOrderByName()
        {
            using (ChinookEntities context = new ChinookEntities())
            {
                var query = from x in context.Tracks
                            orderby x.Name
                            select x;

                return query.ToList();
            }
        }

        public Track GetByPK(int artistId)
        {
            using (ChinookEntities context = new ChinookEntities())
            {
                return context.Tracks.FirstOrDefault(x => x.TrackId == artistId);
            }
        }

        public void DeleteByPK(int artistId)
        {
            Track entity = GetByPK(artistId);

            if (entity == null)
                return;

            Delete(entity);
        }
        public List<Track> Search(string trackName, decimal? minUnitPrice, decimal? maxUnitPrice, int? albumId, int? genreId)
        {
            using (ChinookEntities context = new ChinookEntities())
            {
                List<Artist> artists = context.Artists.ToList();

                IQueryable<Track> query = from x in context.Tracks
                                          select x;

                // lazy execution

                if (string.IsNullOrEmpty(trackName) == false)
                    query = query.Where(x => x.Name.Contains(trackName));

                //if (artistId != null)
                if (minUnitPrice.HasValue)
                    query = query.Where(x => x.UnitPrice > minUnitPrice);
                if (maxUnitPrice.HasValue)
                    query = query.Where(x => x.UnitPrice < maxUnitPrice);
                if (albumId.HasValue)
                    query = query.Where(x => x.AlbumId == albumId);
                if (genreId.HasValue)
                    query = query.Where(x => x.GenreId == genreId);

                var tracks = query.ToList();

                foreach (var track in tracks)
                {
                    track.AlbumTitle = tracks.Find(x => x.AlbumId == track.AlbumId).Name;
                    track.GenreName = tracks.Find(x => x.GenreId == track.GenreId).Name;
                }
                return tracks;

            }
        }
    }
}
