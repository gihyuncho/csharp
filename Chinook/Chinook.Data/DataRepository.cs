﻿namespace Chinook.Data
{
    public class DataRepository
    {
        static DataRepository()
        {
            Album = new AlbumData();
            Artist = new ArtistData();
            Genre = new GenreData();
            Track = new TrackData();
        }

        public static ArtistData Artist { get; }

        public static AlbumData Album { get; }

        public static GenreData Genre { get; }
        public static TrackData Track { get; set; }
    }
}
