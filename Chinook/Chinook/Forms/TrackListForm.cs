﻿using Chinook.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chinook.Forms
{
    public partial class TrackListForm : Form
    {
        public TrackListForm()
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            List<Album> albums = DataRepository.Album.GetAll();

            List<Genre> genres = DataRepository.Genre.GetAll();

            trackSearchControl1.SetDataSources(albums, genres);
        }

        private void trackSearchBtnClicked(object sender, Controls.TrackSearchControl.SearchBtnClickedEventArgs e)
        {
            List<Track> tracks = DataRepository.Track.Search(e.TrackName, e.MinUnitPrice, e.MaxUnitPrice, e.AlbumId, e.GenreId);
            trackListControl1.SetDataSource(tracks);
        }

       
    }
}
