﻿namespace Chinook.Forms
{
    partial class TrackListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.trackListControl1 = new Chinook.Controls.TrackListControl();
            this.trackSearchControl1 = new Chinook.Controls.TrackSearchControl();
            this.SuspendLayout();
            // 
            // trackListControl1
            // 
            this.trackListControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackListControl1.Location = new System.Drawing.Point(0, 101);
            this.trackListControl1.Name = "trackListControl1";
            this.trackListControl1.Size = new System.Drawing.Size(1005, 349);
            this.trackListControl1.TabIndex = 0;
            // 
            // trackSearchControl1
            // 
            this.trackSearchControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.trackSearchControl1.Location = new System.Drawing.Point(0, 0);
            this.trackSearchControl1.Name = "trackSearchControl1";
            this.trackSearchControl1.Size = new System.Drawing.Size(1005, 101);
            this.trackSearchControl1.TabIndex = 1;
            this.trackSearchControl1.SearchBtnClicked += new System.EventHandler<Chinook.Controls.TrackSearchControl.SearchBtnClickedEventArgs>(this.trackSearchBtnClicked);
            // 
            // TrackListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 450);
            this.Controls.Add(this.trackListControl1);
            this.Controls.Add(this.trackSearchControl1);
            this.Name = "TrackListForm";
            this.Text = "TrackListForm";
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.TrackListControl trackListControl1;
        private Controls.TrackSearchControl trackSearchControl1;
    }
}