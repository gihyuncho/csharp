﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Chinook.Data;

namespace Chinook.Controls
{
    public partial class TrackSearchControl : UserControl
    {
        public TrackSearchControl()
        {
            InitializeComponent();
        }

        internal void SetDataSources(List<Album> albums, List<Genre> genres)
        {
            comboBox1.DataSource = albums;
            comboBox2.DataSource = genres;
        }

        public event EventHandler<SearchBtnClickedEventArgs> SearchBtnClicked;

        public class SearchBtnClickedEventArgs
        {
            public string TrackName { get; set; }
            public decimal? MinUnitPrice { get; set; }
            public decimal? MaxUnitPrice { get; set; }
            public int? AlbumId { get; set; }
            public int? GenreId { get; set; }

            public SearchBtnClickedEventArgs(string trackName, decimal? minUnitPrice, decimal? maxUnitPrice, int? albumId,int?genreId)
            {
                TrackName = trackName;
                MinUnitPrice = minUnitPrice;
                MaxUnitPrice = maxUnitPrice;
                AlbumId = albumId;
                GenreId = genreId;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (SearchBtnClicked != null)
            {

                decimal? minUnitPrice = string.IsNullOrWhiteSpace(textBox2.Text) ? (decimal?)null : decimal.Parse(textBox2.Text);
                decimal? maxUnitPrice = string.IsNullOrWhiteSpace(textBox3.Text) ? (decimal?)null : decimal.Parse(textBox3.Text);
                int? albumId = comboBox1.Enabled?(int)comboBox1.SelectedValue:(int?)null;
                int? genreId = comboBox2.Enabled?(int)comboBox2.SelectedValue:(int?)null;
                SearchBtnClicked(this, 
                    new SearchBtnClickedEventArgs(textBox1.Text,minUnitPrice, maxUnitPrice, albumId, genreId));
            }

        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            comboBox1.Enabled = checkBox1.Checked;
        }

        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            comboBox2.Enabled = checkBox2.Checked;
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(comboBox1.SelectedValue.ToString());
        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(comboBox2.SelectedValue.ToString());

        }
    }
}
